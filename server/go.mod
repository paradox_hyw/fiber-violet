module FiberBoot

go 1.16

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/aliyun/aliyun-oss-go-sdk v2.1.10+incompatible
	github.com/arsmn/fiber-swagger/v2 v2.15.0
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/casbin/casbin/v2 v2.35.2
	github.com/casbin/gorm-adapter/v3 v3.3.3
	github.com/cloudflare/tableflip v1.2.2
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/go-redis/redis/v8 v8.11.3
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gofiber/fiber/v2 v2.17.0
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/gookit/color v1.4.2
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/jonboulle/clockwork v0.1.0 // indirect
	github.com/jordan-wright/email v0.0.0-20200824153738-3f5bafa1cd84
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.3 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mojocn/base64Captcha v1.3.5
	github.com/pkg/errors v0.9.1 // indirect
	github.com/qiniu/go-sdk/v7 v7.9.8
	github.com/robfig/cron/v3 v3.0.1
	github.com/satori/go.uuid v1.2.0
	github.com/shirou/gopsutil v3.21.7+incompatible
	github.com/spf13/viper v1.8.1
	github.com/swaggo/swag v1.7.1
	github.com/tebeka/strftime v0.1.3 // indirect
	github.com/tencentyun/cos-go-sdk-v5 v0.7.29
	github.com/tklauser/go-sysconf v0.3.7 // indirect
	github.com/xuri/excelize/v2 v2.4.1
	go.uber.org/zap v1.19.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/tools v0.1.5 // indirect
	gorm.io/driver/mysql v1.1.2
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.13
)
